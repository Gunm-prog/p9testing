package com.dummy.myerp.testbusiness.business;

import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.business.impl.manager.ComptabiliteManagerImpl;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.apache.commons.lang3.ObjectUtils;

import org.junit.jupiter.api.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.*;


@Transactional(propagation = Propagation.REQUIRES_NEW)
/*@ExtendWith(SpringExtension.class)*/
@ContextConfiguration(locations = "classpath:/com/dummy/myerp/business/applicationContext.xml")
@Sql(scripts = {"classpath:/com/dummy/myerp/testbusiness/business/01_create_schema.sql",
                "classpath:/com/dummy/myerp/testbusiness/business/02_create_tables.sql",
                "classpath:/com/dummy/myerp/testbusiness/business/21_insert_data_demo.sql" })
public class ComptabiliteManagerImplIntegrationTest extends BusinessTestCase {

    static ComptabiliteManagerImpl comptabiliteManagerUnderTest;
    EcritureComptable ecritureComptable;
    int currentYear=Calendar.getInstance().get( Calendar.YEAR );

    @BeforeEach
    public void init() {
        comptabiliteManagerUnderTest=new ComptabiliteManagerImpl();
        ecritureComptable=new EcritureComptable();
        ecritureComptable.setJournal( new JournalComptable( "BQ", "Banque" ) );
        ecritureComptable.setDate( new Date() );
        ecritureComptable.setLibelle( "Libelle" );
        ecritureComptable.setReference( "BQ-" + currentYear + "/00001" );
        ecritureComptable.getListLigneEcriture().add( this.createLigne( 401, "200.50", null ) );
        ecritureComptable.getListLigneEcriture().add( this.createLigne( 401, "100.50", "33" ) );
        ecritureComptable.getListLigneEcriture().add( this.createLigne( 411, null, "301" ) );
        ecritureComptable.getListLigneEcriture().add( this.createLigne( 411, "40", "7" ) );
    }


    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit=pDebit == null ? null : new BigDecimal( pDebit );
        BigDecimal vCredit=pCredit == null ? null : new BigDecimal( pCredit );
        String vLibelle=ObjectUtils.defaultIfNull( vDebit, BigDecimal.ZERO )
                .subtract( ObjectUtils.defaultIfNull( vCredit, BigDecimal.ZERO ) ).toPlainString();
        return new LigneEcritureComptable( new CompteComptable( pCompteComptableNumero ),
                vLibelle,
                vDebit, vCredit );
    }

    @Test
    @AfterAll
    static void checkStateOfDataBase() {
        List<EcritureComptable> ecritureComptableList=comptabiliteManagerUnderTest.getListEcritureComptable();
        assertThat( ecritureComptableList.size() ).isEqualTo( 5 );
    }

    @Test
    final void getListCompteComptable() {
        // GIVEN
        List<CompteComptable> compteComptableList;

        // WHEN
        compteComptableList=comptabiliteManagerUnderTest.getListCompteComptable();

        // THEN
        assertThat( compteComptableList ).isNotEmpty();
    }

    @Test
    void getListJournalComptable() {
        // GIVEN
        List<JournalComptable> journalComptableList;

        // WHEN
        journalComptableList=comptabiliteManagerUnderTest.getListJournalComptable();
        System.out.println( journalComptableList );
        // THEN
        assertThat( journalComptableList.size() ).isEqualTo( 4 );
    }


    @Test
    @AfterEach
    void getListEcritureComptable() {
        //GIVEN
        List<EcritureComptable> ecritureComptableList;

        //WHEN
        ecritureComptableList=comptabiliteManagerUnderTest.getListEcritureComptable();
        System.out.println( ecritureComptableList );
        //THEN
        assertThat( ecritureComptableList.size() ).isEqualTo( 5 );
    }


    @Test
    void getListEcritureComptable_insertEcritureComptable_updateEcritureComptable_deleteEcritureComptable() throws FunctionalException, NotFoundException {


        comptabiliteManagerUnderTest.insertEcritureComptable( ecritureComptable );
        try {
            EcritureComptable ecritureComptableFromDb=comptabiliteManagerUnderTest.getEcritureComptable( ecritureComptable.getId() );

            String newLibelle="NewLibelle";
            ecritureComptableFromDb.setLibelle( newLibelle );
            ecritureComptableFromDb.setJournal( new JournalComptable( "BQ", "Banque" ) );
            ecritureComptableFromDb.setReference( "BQ-" + currentYear + "/00011" );
            comptabiliteManagerUnderTest.updateEcritureComptable( ecritureComptableFromDb );

            ecritureComptableFromDb=comptabiliteManagerUnderTest.getEcritureComptable( ecritureComptableFromDb.getId() );
            assertThat( ecritureComptableFromDb.getLibelle() ).isEqualTo( newLibelle );
            assertThat( ecritureComptableFromDb.getReference() ).isEqualTo( "BQ-" + currentYear + "/00011" );

            try {
                comptabiliteManagerUnderTest.deleteEcritureComptable( ecritureComptableFromDb.getId() );
                ecritureComptableFromDb=comptabiliteManagerUnderTest.getEcritureComptable( ecritureComptableFromDb.getId() );
                assertThat( ecritureComptableFromDb ).isNull();
            } catch (NotFoundException e) {
                assertThat( e.getClass() ).isEqualTo( NotFoundException.class );
            }

        } catch (NotFoundException e) {
            assertThat( e.getClass() ).isNotEqualTo( NotFoundException.class );
        }
    }

    @Test
    public void addReference() throws FunctionalException {

        // GIVEN
        ecritureComptable.setReference( null );
        // WHEN
        comptabiliteManagerUnderTest.addReference( ecritureComptable );
        // THEN
        try {
            comptabiliteManagerUnderTest.checkEcritureComptable( ecritureComptable );
        } catch (FunctionalException e) {
            assertThat( e ).hasMessage( e.getMessage() );
        }
    }
}



