package com.dummy.myerp.testbusiness.business;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import static org.junit.jupiter.api.Assertions.assertNotNull;



/**
 * Classe de test de l'initialisation du contexte Spring
 */


@ContextConfiguration(locations = "/com/dummy/myerp/business/applicationContext.xml")
public class TestInitSpring extends BusinessTestCase {

    /**
     * Constructeur.
     */
    public TestInitSpring() {
        super();
    }


    /**
     * Teste l'initialisation du contexte Spring
     */
    @Test
    public void testInit() {
        SpringRegistry.init();
        assertNotNull(SpringRegistry.getBusinessProxy());
        assertNotNull(SpringRegistry.getTransactionManager());
    }
}
