package com.dummy.myerp.consumer.dao.impl.db.dao;

import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.OptionalInt;

import static org.assertj.core.api.Assertions.*;

@Transactional(propagation = Propagation.REQUIRES_NEW)
/*@ExtendWith(SpringExtension.class)*/
@ContextConfiguration(locations = "/com/dummy/myerp/consumer/bootstrapContext.xml")
@Sql(scripts = {"classpath:/com/dummy/myerp/consumer/01_create_schema.sql",
        "classpath:/com/dummy/myerp/consumer/02_create_tables.sql",
        "classpath:/com/dummy/myerp/consumer/21_insert_data_demo.sql" })
public class ComptabiliteDaoImplIntegrationTest extends ConsumerTestCase {

    static ComptabiliteDaoImpl comptabiliteDaoUnderTest;

    JournalComptable journalComptableUnderTest;
    CompteComptable compteComptable1;
    CompteComptable compteComptable2;

    EcritureComptable ecritureComptableUnderTest;

    public OptionalInt getEcritureComptableUnderTestId(){
        if(ecritureComptableUnderTest == null){

            return OptionalInt.empty();
        }else{
            return OptionalInt.of(ecritureComptableUnderTest.getId());
        }
    }

    void haveEcritureComptableUnderTest() throws NotFoundException {
        if(!getEcritureComptableUnderTestId().isPresent()){
            createEcritureComptable();
        }
    }

    @BeforeAll
    public static void testSetupBeforeAll() {
        comptabiliteDaoUnderTest = ComptabiliteDaoImpl.getInstance();
    }

    @Test
    @AfterEach
    void deleteEcritureComptableUnderTest(){

        if (getEcritureComptableUnderTestId().isPresent()){
            try{
                System.out.println(ecritureComptableUnderTest.getId());
                //comptabiliteDaoUnderTest.deleteEcritureComptable( 12);
                comptabiliteDaoUnderTest.deleteEcritureComptable( ecritureComptableUnderTest.getId() );
                EcritureComptable ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptable(ecritureComptableUnderTest.getId() );
                //capture l'exception

                //test si ecritureComptable a été récupéré, malgré la tentative de supression
                assertThat( ecritureComptableFromDB ).isEqualTo( null ); //echec si ecritureComptable non null

            }catch(NotFoundException e){
                assertThat( e.getClass()).isEqualTo( NotFoundException.class );
                //si la classe de mon exception correspond à ma notFoundException ça veut dire que le test est bon
                // parce que ça a été delete l.157
                System.out.println("ecritureUnderTest Deleted id: " + ecritureComptableUnderTest.getId());
            }

        }
    }

    @Test
    @AfterAll
    static void checkStateOfDataBase(){
        List<EcritureComptable> ecritureComptableList = comptabiliteDaoUnderTest.getListEcritureComptable();
        assertThat(ecritureComptableList.size()).isEqualTo( 5 );
    }

    public void init(){
        journalComptableUnderTest = new JournalComptable("BQ",	"Test_Intégration");

        compteComptable1 = new CompteComptable(401, "Fournisseurs");
        compteComptable2 = new CompteComptable(411, "Clients");

        ecritureComptableUnderTest = new EcritureComptable();
        ecritureComptableUnderTest.setJournal(journalComptableUnderTest);

        ecritureComptableUnderTest.setDate(new java.sql.Date(getDateWithoutTimes(new Date()).getTime()));

        ecritureComptableUnderTest.setLibelle("Test_Integration");
        ecritureComptableUnderTest.setReference("BQ-2021/00001");
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(compteComptable1, "200.50", null));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(compteComptable1, "100.50", "33.00"));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(compteComptable2, null, "301"));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(compteComptable2, "40", "7"));

    }
    private LigneEcritureComptable createLigne(CompteComptable compteComptable, String pDebit, String pCredit) {

        /* //todo explanation expression ternaire
        //condition(expression) ternaire (comme un if plus rapide)

        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit)
            equivaut a :
                if(pDebit == null){
                    vDebit = null;
                }else {
                    vDebit = new BigDecimal( pDebit );
                }
        */
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit).setScale(2, RoundingMode.CEILING);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit).setScale(2, RoundingMode.CEILING);

        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();

        return new LigneEcritureComptable(
                compteComptable,
                vLibelle,
                vDebit,
                vCredit
        );
    }
    private Date getDateWithoutTimes(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private void compareEcritureComptable(EcritureComptable initiale, EcritureComptable actual){ //initiale = ecritureComptable inserted actual = extracted ecrCompt
        assertThat( actual.getId() ).isEqualTo( initiale.getId() );
        assertThat( actual.getReference() ).isEqualTo( initiale.getReference() );
        assertThat( actual.getDate() ).isEqualTo( initiale.getDate() );
        assertThat( actual.getLibelle() ).isEqualTo( initiale.getLibelle() );
        assertThat( actual.getJournal().getCode() ).isEqualTo( initiale.getJournal().getCode() );
        assertThat( actual.getListLigneEcriture().size() ).isEqualTo( initiale.getListLigneEcriture().size() );
        assertThat( actual.getTotalCredit() ).isEqualTo( initiale.getTotalCredit() );
        assertThat( actual.getTotalDebit() ).isEqualTo( initiale.getTotalDebit() );
    }

    @Test
    public void getListCompteComptable(){
        List<CompteComptable> compteComptableList = comptabiliteDaoUnderTest.getListCompteComptable();
        System.out.println(compteComptableList);
        assertThat(compteComptableList).isNotEmpty();
    }

    @Test
    void getListJournalComptable() {
        List<JournalComptable> journalComptableList = comptabiliteDaoUnderTest.getListJournalComptable();
        System.out.println(journalComptableList);
        assertThat(journalComptableList).isNotEmpty();
    }


    @Test
    void getListEcritureComptable() {
        List<EcritureComptable> ecritureComptableList = comptabiliteDaoUnderTest.getListEcritureComptable();
        assertThat(ecritureComptableList).isNotEmpty();
    }

    @Test
    void testInsertGetUpdateSequenceEcritureComptable(){

        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(
                "AC",
                2021,
                1
        );

        //insert
        comptabiliteDaoUnderTest.insertSequenceEcritureComptable(
                sequenceEcritureComptable,
                "AC"
        );

        //get
        try{
            // from data demo 'AC',	2016,
            //test getSequenceEcritureComptable
            SequenceEcritureComptable sequenceEcritureComptableFromDb = comptabiliteDaoUnderTest.getSequenceEcritureComptable(
                    "AC",
                    2021
            );
            assertThat( sequenceEcritureComptableFromDb.getAnnee() ).isEqualTo( 2021 );
            assertThat( sequenceEcritureComptableFromDb.getDerniereValeur() ).isEqualTo( 1 );

            //update
            sequenceEcritureComptableFromDb.setDerniereValeur( 2 );
            comptabiliteDaoUnderTest.updateSequenceEcritureComptable(
                    sequenceEcritureComptableFromDb,
                    "AC"
            );

            assertThat(sequenceEcritureComptableFromDb.getDerniereValeur()).isEqualTo( 2 );
        }catch ( NotFoundException exception){
            assertThat( exception.getClass() ).isNotEqualTo( NotFoundException.class );
        }
    }

    @Test
    void createEcritureComptable () throws NotFoundException {
        init();
        //test insert a new ecriture comptable
        comptabiliteDaoUnderTest.insertEcritureComptable(ecritureComptableUnderTest);
        assertThat(ecritureComptableUnderTest.getId()).isNotNull();

        System.out.println("ecritureUnderTest Created id: " + ecritureComptableUnderTest.getId());
    }

    @Test
    void getEcritureComptableByReference () throws NotFoundException {
        haveEcritureComptableUnderTest();

        //test get by ref
        try{
            EcritureComptable ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptableByRef(
                    ecritureComptableUnderTest.getReference()
            );
            compareEcritureComptable( ecritureComptableUnderTest, ecritureComptableFromDB );
        }catch(NotFoundException exception){
            //for failed the test if notFound
            assertThat( exception.getClass()).isNotEqualTo( NotFoundException.class );
        }
    }

    @Test
    void getEcritureComptableById () throws NotFoundException {
        haveEcritureComptableUnderTest();

        //test get by Id
        EcritureComptable ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptable( ecritureComptableUnderTest.getId() );
        compareEcritureComptable( ecritureComptableUnderTest, ecritureComptableFromDB );
    }

    @Test
    void updateEcritureComptable() throws NotFoundException {
        haveEcritureComptableUnderTest();

        EcritureComptable ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptable( ecritureComptableUnderTest.getId() );

        String updatedLibelle = "Nouveau Libelle";
        ecritureComptableFromDB.setLibelle( updatedLibelle );
        comptabiliteDaoUnderTest.updateEcritureComptable( ecritureComptableFromDB );
        ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptable( ecritureComptableFromDB.getId() );
        assertThat( ecritureComptableFromDB.getLibelle() ).isEqualTo( updatedLibelle );
    }
    @Test
    void getListEcritureComptable_addEcritureComptable(){
        init();

        List<EcritureComptable> ecritureComptableList = comptabiliteDaoUnderTest.getListEcritureComptable();

        Integer targetSize = ecritureComptableList.size() +1 ;

        comptabiliteDaoUnderTest.insertEcritureComptable(ecritureComptableUnderTest);
        System.out.println("ecritureUnderTest Created id: " + ecritureComptableUnderTest.getId());

        ecritureComptableList = comptabiliteDaoUnderTest.getListEcritureComptable();

        assertThat(ecritureComptableList.size()).isEqualTo(targetSize);
    }

    @Test
    void getListLigneEcritureComptable( ) throws NotFoundException {
        haveEcritureComptableUnderTest();

        EcritureComptable ecritureComptableFromDB = comptabiliteDaoUnderTest.getEcritureComptable(-1);
        List<LigneEcritureComptable> listLignes =  ecritureComptableFromDB.getListLigneEcriture();

        assertThat(listLignes).isNotEmpty();
    }
}












































