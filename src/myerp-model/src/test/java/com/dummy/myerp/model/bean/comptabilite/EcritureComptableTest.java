package com.dummy.myerp.model.bean.comptabilite;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;


public class EcritureComptableTest {

    EcritureComptable ecritureComptableUnderTest;

    @BeforeEach
    public void init() {
        ecritureComptableUnderTest = new EcritureComptable();
        ecritureComptableUnderTest.setLibelle("Equilibrée");
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(2, "40", "7"));
    }

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                                     .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        return new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                                                                    vLibelle,
                                                                    vDebit, vCredit);
    }

    @Test
    public void checkGetterSetter(){
        EcritureComptable ecritureComptable = new EcritureComptable();

        //id
        ecritureComptable.setId( 25 );
        assertThat( ecritureComptable.getId() ).isEqualTo( 25 );

        //date
        Date date = new java.sql.Date(getDateWithoutTimes(new Date()).getTime());
        ecritureComptable.setDate( date );
        assertThat( ecritureComptable.getDate() ).isEqualTo( date );

        //reference
        ecritureComptable.setReference( "UT-2021/00001" );
        assertThat( ecritureComptable.getReference() ).isEqualTo( "UT-2021/00001" );

        //Libelle
        ecritureComptable.setLibelle( "Unit-test" );
        assertThat( ecritureComptable.getLibelle() ).isEqualTo( "Unit-test" );

        //journal
        ecritureComptable.setJournal( new JournalComptable("UT",	"Unit-test") );
        assertThat( ecritureComptable.getJournal().getCode() ).isEqualTo( "UT" );
    }

    @Test
    public void checkLigneEcritureComptable(){
        LigneEcritureComptable ligneEcritureComptable = new LigneEcritureComptable();

        //Libelle
        ligneEcritureComptable.setLibelle( "UT" );
        assertThat( ligneEcritureComptable.getLibelle() ).isEqualTo( "UT" );

        BigDecimal bigD = new BigDecimal("400.50");
        //Debit
        ligneEcritureComptable.setDebit( bigD );
        assertThat( ligneEcritureComptable.getDebit() ).isEqualTo( bigD );

        //Credit
        ligneEcritureComptable.setCredit( bigD );
        assertThat( ligneEcritureComptable.getCredit() ).isEqualTo( bigD );

        //CompteComptable
        ligneEcritureComptable.setCompteComptable( new CompteComptable(20, "CC") );
        assertThat( ligneEcritureComptable.getCompteComptable().getNumero() ).isEqualTo( 20 );
        assertThat( ligneEcritureComptable.getCompteComptable().getLibelle() ).isEqualTo( "CC" );
    }

    @Test
    public void givenListLigneEcriture_whenGetTotalDebit_thenReturnSumOfDebit (){
        // GIVEN already initialized

        // WHEN
        BigDecimal resultTest = ecritureComptableUnderTest.getTotalDebit();

        // THEN
        assertThat(resultTest.toString()).isEqualTo("341.00");
    }

    @Test
    public void givenListLigneEcriture_whenGetTotalCredit_thenReturnSumOfCredit (){
        // GIVEN already initialized

        // WHEN
        BigDecimal resultTest = ecritureComptableUnderTest.getTotalCredit();

        // THEN
        assertThat(resultTest.toString()).isEqualTo("341");
    }

    @Test
    public void ShouldReturnTrue_WhenTotalCreditIsEqualToTotalDebit() {
        // GIVEN already initialized

        // WHEN
        boolean resultTest = ecritureComptableUnderTest.isEquilibree();

        // THEN
        assertThat(resultTest).isEqualTo(true);
    }

    @Test
    public void ShouldReturnFalse_WhenTotalCreditIsNotEqualToTotalDebit() {
        // GIVEN
        ecritureComptableUnderTest.setLibelle("Non équilibrée");
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(1, "200", null));


        // WHEN
        boolean resultTest = ecritureComptableUnderTest.isEquilibree();

        // THEN
        assertThat(resultTest).isEqualTo(false);
    }

    @Test
    void shouldReturnStringValueOfEcritureComptable_whenToString (){
        // GIVEN
        ecritureComptableUnderTest.getListLigneEcriture().clear();
        ecritureComptableUnderTest.getListLigneEcriture().add(this.createLigne(1, "200.50", null));

        // WHEN
        String result = ecritureComptableUnderTest.toString();

        // THEN
        assertThat(result)
                .isEqualTo("EcritureComptable{id=null, " +
                        "journal=null, " +
                        "reference='null', " +
                        "date=null, " +
                        "libelle='Equilibrée', " +
                        "totalDebit=200.50, " +
                        "totalCredit=0, " +
                        "listLigneEcriture=[\n" +
                            "LigneEcritureComptable{compteComptable=CompteComptable{numero=1, libelle='null'}, " +
                            "libelle='200.50', " +
                            "debit=200.50, " +
                            "credit=null}" +
                        "\n]" +
                        "}");
    }

    private Date getDateWithoutTimes(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
